# Ruby on Rails Code Challenge

Here at Dealer Inspire we write quality code to deliver solutions to the automotive industry.
We'd love your help if you can demonstrate how you handle data in an asynchronous queue-driven web application.

This challenge is composed of 3 parts:

1. Get this starter application running.
2. Grab data from a provided JSON REST HTTP endpoint and store it in a normalized format.
3. Display this information.

## Getting Started

* Install [Docker Desktop](https://www.docker.com/products/docker-desktop)

* cd into this project directory

* Run ```docker-compose build``` to build the containers.

* Run ```docker-compose run web rails db:create``` to create the database

* Run ```docker-compose run web rails db:migrate``` to run database migrations

* Run ```docker-compose run web rails webpacker:install``` to install the front end dependencies

* Run ```docker-compose up -d``` to start the application

* visit localhost:3001

* any additional rails commands that would typically be run locally (such as generator commands) can be run with: ```docker-compose run web your-command-here```

* if you have a local redis-server instance running, you may need to temporarily disable that instance for the docker orchestration to complete.

## Getting Data

In computing, `extract`, `transform`, and `load` (ETL) is the general procedure of copying data from one or more sources into a destination system which represents the data differently from the source(s) or in a different context than the source(s).
The ETL process became a popular concept in the 1970s and is often used in data warehousing.

At DealerInspire, it is common to `extract` data from internal and external systems, `transform` that data to meet our data quality requirements, and then `load` that data into a database.
In some cases we may execute tens of thousands of tasks per day that all follow a common high level goal to consume quality data that can be used to benefit our customers.

With the above in mind, we would like you to use what you know about Object Oriented Programming and Ruby to implement the following:

* Update our `BaseJob` class to form a highly reusable base class from which other classes will be able to orchestrate their ETL operations with minimal code duplication.
* Once you have implemented the base class, create a `DealersJob` worker that utilizes the base class to extract data from a simulated API call returned from our `DealerApiClient#fetch_all` method.
* The `DealersJob` worker should then transform this data in whatever way you think would be most valuable in this system, and then that transformed data should be loaded into a database using `ActiveRecord`. At a minimum you will need to create a dealers table, though some normalization may be ideal in our system. Be sure to create schema migrations and models for any of the core domain classes you create as part of this challenge.

This part may be considered complete once you can invoke the `DealersJob` worker and confirm that data has been loaded in the database in your desired format.

You can run the worker by entering rails console:

    docker-compose run web rails console

and then run the worker:

    DealersJob.new.perform_now

To view an example of the raw data returned from the API, see http://localhost:3001/raw_data/dealers

## Displaying Data

Now that you've successfully implemented your `BaseJob` and `DealersJob` worker classes, it's time to display the loaded data.

Utilize the `LoadedDataController#dealers` method to query the dealers data you have loaded into the database and display that data in a list. Each entry within that list should link to a page that displays a single dealer and any of their related data.

For the purposes of this code challenge, it is acceptable to use server rendered templates (ERB) or front end javascript/react to display this information. (We are a React shop 😉)
