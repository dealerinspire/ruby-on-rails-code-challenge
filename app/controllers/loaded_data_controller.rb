class LoadedDataController < ApplicationController

  # This controller delivers transformed data to the front end.

  def dealers
    raise NotImplementedError, 'This action should return dealers from our database.'
  end
end
