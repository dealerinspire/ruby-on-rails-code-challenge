class RawDataController < ApplicationController

  # This controller delivers raw, untransformed json data to front end for easy inspection

  def dealers
    @dealers = DealerApiClient.fetch_all
    render json: @dealers
  end
end
