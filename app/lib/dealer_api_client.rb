class DealerApiClient
  def self.fetch_all
    sleep 2 # simulate network latency
    [
      {
        name: 'Toyota of Chicago',
        location: 'Chicago, Illinois',
        brand: 'toyota',
        website: 'https://toyota-chicago.com'
      },
      {
        name: 'Mazda of New York',
        location: 'New York, New York',
        brand: 'Mazda',
        website: 'https://mazda-ny.com'

      },
      {
        name: 'Ford of Miami',
        location: 'Miami, Florida',
        brand: 'ford',
        website: 'https://ford-miami.com'

      },
      {
        name: 'Chevy of San Francisco',
        location: 'San Francisco, California',
        brand: 'Chevrolet',
        website: 'https://chevy-sf.com'

      },
      {
        name: 'BMW of San Diego',
        location: 'San Diego, California',
        brand: 'bMW',
        website: 'https://bmw-sd.com'

      },
      {
        name: 'Nissan of Houston',
        location: 'Houston, Texas',
        brand: 'Nissan',
        website: 'https://nissan-houston.com'
      }
    ]
  end
end
