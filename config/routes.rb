Rails.application.routes.draw do
  root 'home#index'

  get '/etl_system', to: 'home#etl_system'
  get '/rails_mvc', to: 'home#rails_mvc'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/raw_data/dealers', to: 'raw_data#dealers'

  get 'loaded_data/dealers', to: 'loaded_data#dealers'
end
